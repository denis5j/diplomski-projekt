﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public static class StaticClass
{
    public static float CrossSceneTimer { get; set; }
    public static TCPManager CrossSceneTCPManager { get; set; }
}

public class StartMenu : MonoBehaviour
{
    public UnityEngine.UI.Button leftButon;
    public UnityEngine.UI.Button rightButon;
    public UnityEngine.UI.Button startButton;
    public UnityEngine.UI.Button exitButton;
    public Text timerText;
    private float timer = 10f;

    void Start()
    {
        UnityEngine.UI.Button lBtn = leftButon.GetComponent<UnityEngine.UI.Button>();
        UnityEngine.UI.Button rBtn = rightButon.GetComponent<UnityEngine.UI.Button>();
        UnityEngine.UI.Button startBtn = startButton.GetComponent<UnityEngine.UI.Button>();
        UnityEngine.UI.Button exitBtn = exitButton.GetComponent<UnityEngine.UI.Button>();
        lBtn.onClick.AddListener(TaskOnClickL);
        rBtn.onClick.AddListener(TaskOnClickR);
        startBtn.onClick.AddListener(TaskOnClickStart);
        exitBtn.onClick.AddListener(TaskOnClickClose);
    }
    
    void TaskOnClickL()
    {
        timer -= 1.0f;
        if(timer < 1.0f)
        {
            timer = 1.0f;
        }
        timerText.text = "" + timer;
    }

    void TaskOnClickR()
    {
        timer += 1.0f;
        if (timer > 20.0f)
        {
            timer = 20.0f;
        }
        timerText.text = "" + timer;
    }

    void TaskOnClickStart()
    {
        StaticClass.CrossSceneTimer = timer;
        StaticClass.CrossSceneTCPManager = FindObjectsOfType<TCPManager>()[0];
        SceneManager.LoadScene("MainScene");
    }

    void TaskOnClickClose()
    {
        Application.Quit();
    }
}
