﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class TCPManager : MonoBehaviour
{
    const string IP = "127.0.0.1";
    const int PORT = 54010;

    TcpClient TCPclient;
    TcpListener TCPlistener;

    Thread tcpThread;

    bool isRunning = true;
    bool sendMessage = false;
    public bool start = false;

    List<string> messagesBuffer = new List<string>();

    void Start()
    {
        tcpThread = new Thread( new ThreadStart(TCPInfo) );
        tcpThread.Start();
    }

    private void TCPInfo()
    {
        TCPlistener = new TcpListener(IPAddress.Parse(IP), PORT);
        TCPlistener.Start();

        TCPclient = TCPlistener.AcceptTcpClient();
        
        while (isRunning)
        {
            ReadStrings();
        }
        TCPlistener.Stop();
    }

    private void ReadStrings()
    {
        NetworkStream nwStream = TCPclient.GetStream();
        byte[] buffer = new byte[TCPclient.ReceiveBufferSize];

        int bytesRecieved = nwStream.Read(buffer, 0, TCPclient.ReceiveBufferSize);
        string data = Encoding.UTF8.GetString(buffer, 0, bytesRecieved);

        if (data != null)
        {
            if (data == "START")
            {
                sendMessage = true;
            }
            else if (data == "STOP")
            {
                isRunning = false;
            }
        }

        if (sendMessage && messagesBuffer.Count > 0)
        {
            sendMessage = false;
            string text = messagesBuffer[0];
            messagesBuffer.Remove(messagesBuffer[0]);
            buffer = System.Text.Encoding.UTF8.GetBytes(text);
            nwStream.Write(buffer, 0, buffer.Length);
        }
    }

    public void WriteStrings(string text)
    {
        if (sendMessage)
        {
            NetworkStream nwStream = TCPclient.GetStream();
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(text);
            nwStream.Write(buffer, 0, buffer.Length);
            sendMessage = false;
        }
        else
        {
            messagesBuffer.Add(text);
        }
    }
}
