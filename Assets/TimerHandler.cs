﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimerHandler : MonoBehaviour
{
    public Text timerText;
    public float timer;

    void Start()
    {
        timer = StaticClass.CrossSceneTimer * 60;
    }

    void Update()
    {
        timer -= Time.deltaTime;
        timerText.text = (int)(timer / 60) + ":" + (int)(timer % 60);
        if (timer < 0)
        {
            Application.Quit();
            SceneManager.LoadScene("StartScene");
        }
    }
}
