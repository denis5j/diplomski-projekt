﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{

    public UnityEngine.UI.Button leftButon;
    public UnityEngine.UI.Button rightButon;
    public ControllableAirplane avion;

    TextHandler textHandler;

    void Start()
    {
        textHandler = FindObjectsOfType<TextHandler>()[0];
        UnityEngine.UI.Button lBtn = leftButon.GetComponent<UnityEngine.UI.Button>();
        UnityEngine.UI.Button rBtn = rightButon.GetComponent<UnityEngine.UI.Button>();
        lBtn.onClick.AddListener(TaskOnClickL);
        rBtn.onClick.AddListener(TaskOnClickR);
    }
    void TaskOnClickL()
    {
        textHandler.PlaneClickedDirection(avion,true);
        avion.StartAnimation(true);
    }
    void TaskOnClickR()
    {
        textHandler.PlaneClickedDirection(avion, false);
        avion.StartAnimation(false);
    }
}