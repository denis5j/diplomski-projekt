﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UncontrollableAirplane : MonoBehaviour
{
    public float movementSpeed = 0.3f;
    public float currentAngle = 0f;
    public string airplaneName;
    public GameObject AirplanePicture;

    TextHandler textHandler;

    void Start()
    {
        textHandler = FindObjectsOfType<TextHandler>()[0];
    }

    void Update()
    {
        UpdatePosition();
    }

    public void UpdatePosition()
    {
        Vector3 relativePosition = new Vector3(Mathf.Sin(currentAngle / 180 * Mathf.PI) * movementSpeed * Time.deltaTime, Mathf.Cos(currentAngle / 180 * Mathf.PI) * movementSpeed * Time.deltaTime, 0);
        transform.position = transform.position + relativePosition;

        Quaternion rotation = Quaternion.LookRotation(relativePosition, new Vector3(0, 0, -1));
        rotation.x = 0;
        rotation.y = 0;
        AirplanePicture.transform.rotation = rotation;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name.Contains("NAV"))
        {
            textHandler.PlaneColidedWithCheckpoint(this, other.gameObject.name);
        }
        else if (other.gameObject.name.Contains("ControllableAirplane"))
        {
            textHandler.PlaneColidedWithAirplane(this, other.gameObject.GetComponent<ControllableAirplane>());
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name.Contains("NAV"))
        {
            textHandler.PlaneExitCollisionWithCheckpoint(this, other.gameObject.name);
        }
        else if (other.gameObject.name.Contains("ControllableAirplane"))
        {
            textHandler.PlaneExitCollisionWithAirplane(this, other.gameObject.GetComponent<ControllableAirplane>());
        }
    }
}