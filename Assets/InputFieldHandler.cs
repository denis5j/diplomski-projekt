﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InputFieldHandler : MonoBehaviour
{
    public ControllableAirplane airplane;
    InputField airplaneInputField;

    public void Start()
    {
        airplaneInputField = gameObject.GetComponent<InputField>();
        airplaneInputField.onEndEdit.AddListener(delegate { LockInput(airplaneInputField); });
    }

    void LockInput(InputField input)
    {
        if (input.text.Length <= 0)
        {
            return;
        }
        int angle = System.Convert.ToInt32(airplaneInputField.text);
        switch (input.text.Length)
        {
            case 1:
                angle *= 100;
                break;
            case 2:
                angle *= 10;
                break;
            default:
                break;
        }
        angle %= 360;
        airplane.wantedAngle = angle;
        input.text = angle.ToString("D3");
    }
}