﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllableAirplane : UncontrollableAirplane
{
    public Text currentDirectionText;
    public Text nameTekst;
    public List<GameObject> checkpoints;
    public float wantedAngle = 0f;

    bool leftAnimationActive = false;
    bool rightAnimationActive = false;
    
    float animationTime = 0f;
    public float rotationalVelocity = 16f;
    TextHandler textHandler;
    
    void Start()
    {
        textHandler = FindObjectsOfType<TextHandler>()[0];
        nameTekst.text = airplaneName;
    }
    
    void Update()
    {
        if (leftAnimationActive)
        {
            PlayAnimation(true);
        }
        else if (rightAnimationActive)
        {
            PlayAnimation(false);
        }

        UpdatePosition();

        currentDirectionText.text =(((int)currentAngle+360) % 360).ToString("D3");
    }

    void PlayAnimation(bool left)
    {
        float time = Time.deltaTime;
        currentAngle -= ( left? 1 :-1 ) * time * rotationalVelocity;
        animationTime -= time;

        if (animationTime <= 0f)
        {
            currentAngle = wantedAngle;
            leftAnimationActive = false;
            rightAnimationActive = false;
        }
    }

    public void StartAnimation(bool left)
    {
        float angleBetweenCurrentAndWanted;
        leftAnimationActive = left;
        rightAnimationActive = !left;
        angleBetweenCurrentAndWanted = 
            left ? 
            (
                currentAngle < wantedAngle ?
                360 - wantedAngle + currentAngle :
                currentAngle - wantedAngle
            ) : 
            (
                currentAngle > wantedAngle ?
                360 - currentAngle + wantedAngle:
                wantedAngle - currentAngle
            );
        animationTime = angleBetweenCurrentAndWanted / rotationalVelocity;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name.Contains("NAV"))
        {
            textHandler.PlaneColidedWithCheckpoint(this, other.gameObject.name);
        }
        else if (other.gameObject.name.Contains("ControllableAirplane"))
        {
            textHandler.PlaneColidedWithAirplane(this, other.gameObject.GetComponent<ControllableAirplane>());
        }
        else if (other.gameObject.name.Contains("UncontrollableAirplane"))
        {
            textHandler.PlaneColidedWithAirplane(this, other.gameObject.GetComponent<UncontrollableAirplane>());
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name.Contains("NAV"))
        {
            textHandler.PlaneExitCollisionWithCheckpoint(this, other.gameObject.name);
        }
        else if (other.gameObject.name.Contains("ControllableAirplane")) //or uncontrollabel
        {
            textHandler.PlaneExitCollisionWithAirplane(this, other.gameObject.GetComponent<ControllableAirplane>());
        }
        else if (other.gameObject.name.Contains("UncontrollableAirplane"))
        {
            textHandler.PlaneExitCollisionWithAirplane(this, other.gameObject.GetComponent<UncontrollableAirplane>());
        }
    }
}