﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Text;
using UnityEngine.SceneManagement;


public class TextHandler : MonoBehaviour
{
    public TextAsset textAsset;
    public List<GameObject> checkpoint;
    public List<ControllableAirplane> airplane;
    public List<UncontrollableAirplane> uncontrollableAirplane;
    TCPManager tcpManager;
    string timeAndDate;
    string path = "Assets/Resources/Output.txt";

    void Start()
    {
        tcpManager = StaticClass.CrossSceneTCPManager;
        CheckpointStart();
        AirplaneStart();
        UncontrollableAirplaneStart();
        timeAndDate = System.DateTime.Now.ToString("yyyy-MM-dd ") + System.DateTime.Now.ToString("HH:mm:ss;");
        string stringToSend = timeAndDate + "StartEnd";
        WriteString(stringToSend);
        tcpManager.WriteStrings(stringToSend);
        InvokeRepeating("Airplane", 0, 0.1f);

    }

    public void Airplane()
    {
        AirplaneUpdate();
        UncontrollableAirplaneUpdate();
    }

    public void UncontrollableAirplaneUpdate()
    {
        string stringToSend;
        for (int i = 0; i < uncontrollableAirplane.Count; i++)
        {
            timeAndDate = System.DateTime.Now.ToString("yyyy-MM-dd ") + System.DateTime.Now.ToString("HH:mm:ss;");
            stringToSend = timeAndDate + "UncontrollableUpdate;" + uncontrollableAirplane[i].airplaneName + ";CurrentSpeed:"+uncontrollableAirplane[i].movementSpeed.ToString("0.00").Replace(',', '.') + ";CurrHeading:"+ (int)uncontrollableAirplane[i].currentAngle+";" + "CurrPosition;" + 
                "X=" + uncontrollableAirplane[i].transform.position.x.ToString("0.00").Replace(',', '.') + "," +
                "Y=" + uncontrollableAirplane[i].transform.position.y.ToString("0.00").Replace(',', '.') + "," +
                "Z=" + uncontrollableAirplane[i].transform.position.z.ToString("0.00").Replace(',', '.');
            WriteString(stringToSend);
            tcpManager.WriteStrings(stringToSend); 
        }
        timeAndDate = System.DateTime.Now.ToString("yyyy-MM-dd ") + System.DateTime.Now.ToString("HH:mm:ss;");
        stringToSend = timeAndDate + "AllAirplanesUpdated;";
        WriteString(stringToSend);
        tcpManager.WriteStrings(stringToSend);
    }

    public void AirplaneUpdate()
    {
        for (int i = 0; i < airplane.Count; i++)
        {
            timeAndDate = System.DateTime.Now.ToString("yyyy-MM-dd ") + System.DateTime.Now.ToString("HH:mm:ss;");
            string stringToSend = timeAndDate + "AirplaneUpdate;" + airplane[i].airplaneName + ";CurrentSpeed:" + airplane[i].movementSpeed.ToString("0.00").Replace(',', '.') + ";CurrHeading:" + (int)airplane[i].currentAngle + ";" + "CurrPosition;" +
                "X=" + airplane[i].transform.position.x.ToString("0.00").Replace(',', '.') + "," +
                "Y=" + airplane[i].transform.position.y.ToString("0.00").Replace(',', '.') + "," +
                "Z=" + airplane[i].transform.position.z.ToString("0.00").Replace(',', '.');
            WriteString(stringToSend);
            tcpManager.WriteStrings(stringToSend);
        }
    }

    void CheckpointStart()
    {
        for (int i = 0; i < checkpoint.Count; i++)
        {
            timeAndDate = System.DateTime.Now.ToString("yyyy-MM-dd ") + System.DateTime.Now.ToString("HH:mm:ss;");
            string stringToSend = timeAndDate + "NavigationPoint" + i + ";" + "CurrPosition;" +
                "X=" + checkpoint[i].transform.position.x.ToString("0.00").Replace(',', '.') + "," +
                "Y=" + checkpoint[i].transform.position.y.ToString("0.00").Replace(',', '.') + "," +
                "Z=" + checkpoint[i].transform.position.z.ToString("0.00").Replace(',', '.');
            WriteString(stringToSend);
            tcpManager.WriteStrings(stringToSend);
        }
    }

    public void AirplaneStart()
    {
        for (int i = 0; i < airplane.Count; i++)
        {
            timeAndDate = System.DateTime.Now.ToString("yyyy-MM-dd ") + System.DateTime.Now.ToString("HH:mm:ss;");
            string stringToSend = timeAndDate + "AirplaneStart;" + airplane[i].airplaneName+";";
            for (int j = 0; j < checkpoint.Count; j++) 
            {
                Text checkpointName = airplane[i].checkpoints[j].GetComponentInChildren<Text>();
                stringToSend += checkpointName.text;
                if( j != checkpoint.Count - 1 ) stringToSend += "-";
            }
            stringToSend += ";CurrPosition;" + 
                "X=" + airplane[i].transform.position.x.ToString("0.00").Replace(',', '.') + "," +
                "Y=" + airplane[i].transform.position.y.ToString("0.00").Replace(',', '.') + "," +
                "Z=" + airplane[i].transform.position.z.ToString("0.00").Replace(',', '.');
            WriteString(stringToSend);
            tcpManager.WriteStrings(stringToSend);
        }
    }

    public void UncontrollableAirplaneStart()
    {
        for (int i = 0; i < uncontrollableAirplane.Count; i++)
        {
            timeAndDate = System.DateTime.Now.ToString("yyyy-MM-dd ") + System.DateTime.Now.ToString("HH:mm:ss;");
            string stringToSend = timeAndDate + "UncontrollableStart;" + uncontrollableAirplane[i].airplaneName + ";";
            stringToSend += "CurrPosition;" +
                "X=" + uncontrollableAirplane[i].transform.position.x.ToString("0.00").Replace(',', '.') + "," +
                "Y=" + uncontrollableAirplane[i].transform.position.y.ToString("0.00").Replace(',', '.') + "," +
                "Z=" + uncontrollableAirplane[i].transform.position.z.ToString("0.00").Replace(',', '.');
            WriteString(stringToSend);
            tcpManager.WriteStrings(stringToSend);
        }
    }

    public void PlaneClickedDirection(ControllableAirplane airplane, bool left)
    {
        timeAndDate = System.DateTime.Now.ToString("yyyy-MM-dd ") + System.DateTime.Now.ToString("HH:mm:ss;");
        string stringToSend = timeAndDate + "HeadingSelected;" +airplane.airplaneName+"; "+ "HeadingSelectorLog;" + airplane.wantedAngle + ";"+left;
        WriteString(stringToSend);
        tcpManager.WriteStrings(stringToSend);
    }

    public void PlaneColidedWithCheckpoint(UncontrollableAirplane airplane, string checkpointName)
    {
        timeAndDate = System.DateTime.Now.ToString("yyyy-MM-dd ") + System.DateTime.Now.ToString("HH:mm:ss;");
        string stringToSend = timeAndDate + "CollisionWithCheckpoint;" + airplane.airplaneName + ";" + checkpointName + ";";
        WriteString(stringToSend);
        tcpManager.WriteStrings(stringToSend);
    }

    public void PlaneColidedWithAirplane(UncontrollableAirplane airplane, UncontrollableAirplane airplane2)
    {
        timeAndDate = System.DateTime.Now.ToString("yyyy-MM-dd ") + System.DateTime.Now.ToString("HH:mm:ss;");
        string stringToSend = timeAndDate + "CollisionWithPlane;" + airplane.airplaneName + ";" + airplane2.airplaneName + ";";
        WriteString(stringToSend);
        tcpManager.WriteStrings(stringToSend);
    }

    public void PlaneExitCollisionWithAirplane(UncontrollableAirplane airplane, UncontrollableAirplane airplane2)
    {
        timeAndDate = System.DateTime.Now.ToString("yyyy-MM-dd ") + System.DateTime.Now.ToString("HH:mm:ss;");
        string stringToSend = timeAndDate + "ColisionFinishedAirplane;"+ airplane.airplaneName + ";" + airplane2.airplaneName + ";";
        WriteString(stringToSend);
        tcpManager.WriteStrings(stringToSend);
    }

    public void PlaneExitCollisionWithCheckpoint(UncontrollableAirplane airplane, string checkpointName)
    {
        timeAndDate = System.DateTime.Now.ToString("yyyy-MM-dd ") + System.DateTime.Now.ToString("HH:mm:ss;");
        string stringToSend = timeAndDate + "ColisionFinishedCheckpoint;" + airplane.airplaneName + ";" + checkpointName + ";";
        WriteString(stringToSend);
        tcpManager.WriteStrings(stringToSend);
    }

    public void WriteString(string textToWrite)
    {
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(textToWrite);
        writer.Close();
    }
    
}