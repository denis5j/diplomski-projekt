from cmath import cos, sin
import math
from re import A
from tabnanny import check
import numpy as np
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import socket
from multiprocessing import Process
import atexit
import time
from datetime import datetime
import numpy as np
from multiprocessing import shared_memory
from multiprocessing import Array, Lock

HOST = "127.0.0.1"  # The server's hostname or IP address
PORT = 54010  # The port used by the server
PATH = "output.txt"


trasholdTTGC = 10

import math

def distanceBetweenTwoPoints(t1,t2):
    return math.sqrt(pow(t1[0]-t2[0],2)+pow(t1[1]-t2[1],2)+pow(t1[2]-t2[2],2))

alfa = 0.002
beta = 0.01

def weightingFunction(distance):
    return (pow(math.e,-alfa*pow(distance,2))+pow(math.e,-beta*distance))/2

def isAirplaneInsideOfScene(position):
    return position[0]> -8.5 and position[0] < 8.5 and position[1]> -5.2 and position[1] < 5.2 

def vijFunction(head1,position1,head2,position2,distance,speed1,speed2):
    x1 = math.sin(head1)*speed1
    y1 = math.cos(head1)*speed1
    x2 = math.sin(head2)*speed2
    y2 = math.cos(head2)*speed2
    x3 = position2[0] - position1[0]
    y3 = position2[1] - position1[1]
    x4 = x2 - x1
    y4 = y2 - y1
    return (x3*x4 + y3*y4) / (distance)

def dataProcessing(values, lock):
    f = open(PATH, "w")
    
    traveledDistancePerPlane=[]
    idealTraveledDistancePerPlane=[]
    navigationPointsLocations =[]
    airplaneStartLocations =[]
    idealDistances =[]
    idealStartDistances =[]
    isFirstCheckpoint = []
    airplaneLastPosition=[]
    airplaneCheckpointsOrder=[]
    airplaneCheckpointsOrdercurrentIndex=[]
    airplaneChangedDirection=[]
    airplaneCrashCount=0
    airplaneCheckpointCount=[]
    airplaneAirtmCheckpointCount=0
    airplaneCount=0
    uncontrollableAirplaneCount=0
    uncontrollableAirplaneLastPosition = []
    airplaneDistanceRation=[]
    airplanearitmDistanceRation=0
    airplaneStartConflict=[]
    airplaneDurationInConflict=0
    airplaneCurrentHeading=[]
    uncontrollableAirplaneCurrentHeading=[]
    airplaneCurrentSpeed=[]
    uncontrollableAirplaneCurrentSpeed=[]
    minimalDistance = 0
    arithmeticDistance = 0

    divergence=0
    convergence=0
    timeToGoToConflict = []

    def exit_handler():  
        soc.sendall(b"STOP")
        f.close()


    atexit.register(exit_handler)
    
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    soc.connect((HOST, PORT))
    while(True):
        soc.sendall(b"START")
        data = soc.recv(128)
        message = data.decode()
        #print(message)
        f.write(message)
        if(len(data)):
            messageParts = message.split(';')
            if("NavigationPoint" in message):
                positions = messageParts[len(messageParts) - 1].split(',')
                vektorPositions = []
                for idx, position in enumerate(positions):
                    sliced = position[2:]
                    vektorPositions.append(float(sliced))
                navigationPointsLocations.append(vektorPositions)

            if("StartEnd" in message):
                for idx, navigationPointsLocation in enumerate(navigationPointsLocations):
                    index = (idx-1+len(navigationPointsLocations))%len(navigationPointsLocations)
                    idealDistances.append(distanceBetweenTwoPoints(navigationPointsLocation,navigationPointsLocations[index])-0.3)
                for idx, airplaneStartLocation in enumerate(airplaneStartLocations):
                    idealStartDistances.append(distanceBetweenTwoPoints(airplaneStartLocation,navigationPointsLocations[airplaneCheckpointsOrder[idx][airplaneCheckpointsOrdercurrentIndex[idx]]])-0.3)
                print(navigationPointsLocations,idealDistances)
            if("AirplaneStart" in message):
                positions = messageParts[len(messageParts) - 1].split(',')
                vektorPositions = []
                for idx,position in enumerate(positions):
                    sliced = position[2:]
                    vektorPositions.append(float(sliced))
                airplaneStartLocations.append(vektorPositions)
                airplaneLastPosition.append(vektorPositions)
                
                navpoints = messageParts[3].split('-')
                airOrder = []
                for idx, navpoint in enumerate(navpoints) :
                    airOrder.append(int(navpoint[4:])-1)
                airplaneCheckpointsOrder.append(airOrder)
                airplaneCheckpointsOrdercurrentIndex.append(0)
                airplaneChangedDirection.append(0)
                traveledDistancePerPlane.append(0)
                airplaneCheckpointCount.append(0)
                isFirstCheckpoint.append(True)
                airplaneDistanceRation.append(0)
                airplaneCurrentHeading.append(0)
                airplaneCurrentSpeed.append(0.0)
                idealTraveledDistancePerPlane.append(0)
                airplaneStartConflict.append(datetime.fromisoformat("2022-05-24 19:37:45"))
                airplaneCount+=1
            if("UncontrollableStart" in message):
                uncontrollableAirplaneCount+=1
                positions = messageParts[len(messageParts) - 1].split(',')
                vektorPositions = []
                for idx,position in enumerate(positions):
                    sliced = position[2:]
                    vektorPositions.append(float(sliced))
                uncontrollableAirplaneLastPosition.append(vektorPositions)
                
                uncontrollableAirplaneCurrentHeading.append(0)
                uncontrollableAirplaneCurrentSpeed.append(0.0)
            if("UncontrollableUpdate" in message):
                positions = messageParts[len(messageParts) - 1].split(',')
                vektorPositions = []
                heading = int(messageParts[len(messageParts) - 3].split(':')[1])
                speed = float(messageParts[len(messageParts) - 4].split(':')[1])
                for idx,position in enumerate(positions):
                    sliced = position[2:]
                    vektorPositions.append(float(sliced))
                
                airplaneID = int(messageParts[2][3:]) - 1
                uncontrollableAirplaneLastPosition[airplaneID]=vektorPositions
                uncontrollableAirplaneCurrentHeading[airplaneID]=heading
                uncontrollableAirplaneCurrentSpeed[airplaneID] = speed
            
            if("AllAirplanesUpdated" in message):
                vij = np.zeros((airplaneCount,airplaneCount))
                vij2 = np.zeros((airplaneCount,uncontrollableAirplaneCount))
                timeToGoToConflict = []
                for i in range(airplaneCount):
                    for j in range(airplaneCount):
                        if(i <= j):
                            continue
                        head1 = airplaneCurrentHeading[i]
                        head2 = airplaneCurrentHeading[j]
                        udaljenostIJ = distanceBetweenTwoPoints(airplaneLastPosition[i],airplaneLastPosition[j])
                        if(udaljenostIJ < 0.0001):
                            udaljenostIJ = 0.0001
                        vij[i][j]=vijFunction(head1, airplaneLastPosition[i],head2,airplaneLastPosition[j],udaljenostIJ,airplaneCurrentSpeed[i],airplaneCurrentSpeed[j])
                        if(head1 == head2):
                            timeToGoToConflict.append(trasholdTTGC)
                        else:
                            timeToGoToConflict.append(-(udaljenostIJ)/(vij[i][j]))

                for i in range(airplaneCount):
                    for j in range(uncontrollableAirplaneCount):
                        if(not isAirplaneInsideOfScene(uncontrollableAirplaneLastPosition[j])):
                            continue

                        head1 = airplaneCurrentHeading[i]
                        head2 = uncontrollableAirplaneCurrentHeading[j]
                        udaljenostIJ = distanceBetweenTwoPoints(airplaneLastPosition[i],uncontrollableAirplaneLastPosition[j])
                        if(udaljenostIJ == 0.0):
                            udaljenostIJ = 0.0001
                        vij2[i][j]=vijFunction(head1, airplaneLastPosition[i],head2,uncontrollableAirplaneLastPosition[j],udaljenostIJ,airplaneCurrentSpeed[i],uncontrollableAirplaneCurrentSpeed[j])
                        if(head1 == head2):
                            timeToGoToConflict.append(trasholdTTGC)
                        else:
                            timeToGoToConflict.append(-(udaljenostIJ)/(vij2[i][j]))
                        
                divergence = 0
                convergence = 0
                numberOfPlanes = 0
                first = True
                for i in range(airplaneCount):
                    for j in range(airplaneCount):
                        if(i == j):
                            continue
                        udaljenostIJ = distanceBetweenTwoPoints(airplaneLastPosition[i],airplaneLastPosition[j])
                        if(udaljenostIJ == 0.0):
                            udaljenostIJ = 0.0001

                        arithmeticDistance += udaljenostIJ
                        if(first):
                            minimalDistance = udaljenostIJ
                            first = False
                        minimalDistance = min(minimalDistance, udaljenostIJ)

                        if(vij[i][j]<0):
                            divergence+=abs(vij[i][j])*weightingFunction(udaljenostIJ)
                        else:
                            convergence+=abs(vij[i][j])*weightingFunction(udaljenostIJ)
                        numberOfPlanes+=1
                
                for i in range(airplaneCount):
                    for j in range(uncontrollableAirplaneCount):
                        udaljenostIJ = distanceBetweenTwoPoints(airplaneLastPosition[i],uncontrollableAirplaneLastPosition[j])
                        if(udaljenostIJ == 0.0):
                            udaljenostIJ = 0.0001
                        
                        if(not isAirplaneInsideOfScene(uncontrollableAirplaneLastPosition[j])):
                            continue

                        minimalDistance = min(minimalDistance, udaljenostIJ)
                        arithmeticDistance += udaljenostIJ

                        if(vij2[i][j]<0):
                            divergence+=abs(vij2[i][j])*weightingFunction(udaljenostIJ)
                        else:
                            convergence+=abs(vij2[i][j])*weightingFunction(udaljenostIJ)
    
                        numberOfPlanes+=1
                        
                first = False

                urgentCounter = 0
                for val in timeToGoToConflict:
                    if 0 < val and val < trasholdTTGC:
                        urgentCounter+=1
                
                divergence/=(numberOfPlanes-airplaneCount)
                convergence/=(numberOfPlanes-airplaneCount)
                arithmeticDistance/=numberOfPlanes
                lock.acquire()
                values[10] = minimalDistance
                values[11] = arithmeticDistance
                values[12] = convergence
                values[13] = divergence
                values[14] = urgentCounter/len(timeToGoToConflict)
                lock.release()

            if("AirplaneUpdate" in message):
                positions = messageParts[len(messageParts) - 1].split(',')
                heading = int(messageParts[len(messageParts) - 3].split(':')[1])
                speed = float(messageParts[len(messageParts) - 4].split(':')[1])
                airplaneID = int(messageParts[2][3:]) - 1
                vektorPositions = []
                for idx, position in enumerate(positions):
                    position
                    sliced = position[2:]
                    vektorPositions.append(float(sliced))
                traveledDistancePerPlane[airplaneID] += distanceBetweenTwoPoints(vektorPositions,airplaneLastPosition[airplaneID])
                airplaneLastPosition[airplaneID] = vektorPositions
                airplaneCurrentHeading[airplaneID] = heading
                airplaneCurrentSpeed[airplaneID] = speed
                
            if("HeadingSelected" in message):
                airplaneChangedDirection[int(messageParts[2][3:])-1]+=1
                lock.acquire()
                values[airplaneCount+airplaneCount+3]=0
                for countDir in airplaneChangedDirection:
                    values[airplaneCount+airplaneCount+3] +=countDir
                lock.release()
            
            if("CollisionWithCheckpoint" in message):
                airplaneIndex = int(messageParts[2][3:])-1
                checkpointIndex = int(messageParts[3][4:])-1
                if checkpointIndex == airplaneCheckpointsOrder[airplaneIndex][airplaneCheckpointsOrdercurrentIndex[airplaneIndex]]:
                    airplaneCheckpointCount[airplaneIndex]+=1
                    airplaneCheckpointsOrdercurrentIndex[airplaneIndex]+=1
                    airplaneCheckpointsOrdercurrentIndex[airplaneIndex]%=len(airplaneCheckpointsOrder[airplaneIndex])
                    lock.acquire()
                    values[airplaneIndex+1]+=1
                    airplaneAirtmCheckpointCount+=1/airplaneCount
                    values[airplaneCount+1] = airplaneAirtmCheckpointCount
                    lock.release()
                    
                    if isFirstCheckpoint[airplaneIndex]:
                        idealTraveledDistancePerPlane[airplaneIndex]+=idealStartDistances[airplaneIndex]
                        isFirstCheckpoint[airplaneIndex]=False
                    else:
                        if(airplaneIndex != 0):
                            idealTraveledDistancePerPlane[airplaneIndex]+=idealDistances[checkpointIndex]
                        else:
                            idealTraveledDistancePerPlane[airplaneIndex]+=idealDistances[(checkpointIndex+1)%len(idealDistances)]
                    airplaneDistanceRation[airplaneIndex]=idealTraveledDistancePerPlane[airplaneIndex]/traveledDistancePerPlane[airplaneIndex]

                    airplanearitmDistanceRation=0
                    for distance in airplaneDistanceRation:
                        airplanearitmDistanceRation+=distance
                    airplanearitmDistanceRation/=airplaneCount
                    lock.acquire()
                    values[airplaneCount+2+airplaneIndex] = airplaneDistanceRation[airplaneIndex]
                    values[airplaneCount+2+airplaneCount] = airplanearitmDistanceRation
                    lock.release()

            if("CollisionWithPlane" in message):
                airplaneIndex = int(messageParts[2][3:])-1
                airplaneStartConflict[airplaneIndex] = datetime.fromisoformat(messageParts[0])
                airplaneCrashCount+=0.5
                lock.acquire()
                values[0] =  airplaneCrashCount
                lock.release()

            if("ColisionFinishedAirplane" in message):
                airplaneIndex = int(messageParts[2][3:])-1
                airplaneDurationInConflict += (datetime.fromisoformat(messageParts[0]) - airplaneStartConflict[airplaneIndex]).total_seconds()/2
                lock.acquire()
                values[15] = airplaneDurationInConflict
                lock.release()
        
        time.sleep(0.001)

def GUI(values,lock):
    fig = plt.figure(figsize=(10, 10))

    tit = ['Ncoll','Ncpt.A','Ncpt.B','Ncpt.C','Mcpt','Rtry.A','Rtry.B','Rtry.C','MRtry','Nhead','MINd','Md','MConv','MDiv', 'MTCol', 'TCol']
    lab = [
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345'),
        list('12345')
    ]
    maxval= [
        5,
        5,
        5,
        5,
        5,
        1,
        1,
        1,
        1,
        20,
        20,
        20,
        1,
        1,
        1,
        15
    ]

    maxinc = [
        3,
        3,
        3,
        3,
        3,
        0,
        0,
        0,
        0,
        10,
        5,
        5,
        0,
        0,
        0,
        5
    ]


    radar = Radar(fig, tit, lab)
    lock.acquire()
    ln, = radar.plot(values,  '-', lw=2, color='b', alpha=0.4, label='')
    lock.release()
    radar.ax.legend()

    def updateLabels():
        for idx, val in enumerate(maxval):
            for i in range(1,6):
                radar.labels[idx][i-1]= "{:.1f}".format((val/5)*i)
                lab[idx][i-1]= "{:.1f}".format((val/5)*i)

        for ax, angle, label in zip(radar.axes, radar.angles, radar.labels):
            ax.set_rgrids(range(1, 6), angle=angle, labels=label)
            ax.spines['polar'].set_visible(False)
            ax.set_ylim(0, 6) 
    
    def updateRadarChart(frame):
     
        xdata = np.deg2rad(np.r_[radar.angles, radar.angles[0]])
        lock.acquire()
        
        for idx in range(len(values)):
            if(maxval[idx]<values[idx]):
                maxval[idx]+=maxinc[idx]
        
        updateLabels()
        
        ydata = np.r_[values, values[0]]
        for idx in range(len(ydata)-1):
            ydata[idx]/=maxval[idx]/5
            
        ydata[len(ydata)-1]/=maxval[0]/5
        lock.release()
        ln.set_data(xdata,ydata)

        return ln,

    ani = animation.FuncAnimation(fig, updateRadarChart, interval=10, blit=False)
    plt.show()

class Radar(object):
    def __init__(self, figure, title, labels, rect=None):
        self.labels = labels
        self.title = title
        if rect is None:
            rect = [0.05, 0.05, 0.9, 0.9]

        self.n = len(title)
        self.angles = np.arange(0, 360, 360.0/self.n)

        self.axes = [figure.add_axes(rect, projection='polar', label='axes%d' % i) for i in range(self.n)]

        self.ax = self.axes[0]
        self.ax.set_thetagrids(self.angles, labels=title, fontsize=14)

        for ax in self.axes[1:]:
            ax.patch.set_visible(False)
            ax.grid(False)
            ax.xaxis.set_visible(False)


        for ax, angle, label in zip(self.axes, self.angles, self.labels):
            ax.set_rgrids(range(1, 6), angle=angle, labels=label)
            ax.spines['polar'].set_visible(False)
            ax.set_ylim(0, 6) 

    def plot(self, values, *args, **kw):
        angle = np.deg2rad(np.r_[self.angles, self.angles[0]])
        values = np.r_[values, values[0]]
        ln, = self.ax.plot(angle, values, *args, **kw)
        return ln, 


if __name__ == '__main__':
    values = Array('d', range(16))
    lock=Lock()
    for val,idx in enumerate(values):
        values[int(idx)]=0
    dataProcess = Process(target=dataProcessing, args=(values,lock,))
    dataProcess.start()
    GUIProcess = Process(target=GUI, args=(values,lock,))
    GUIProcess.start()
    
    dataProcess.join()
    GUIProcess.join()
